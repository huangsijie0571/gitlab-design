window.__imported__ = window.__imported__ || {};
window.__imported__["repo-mirroring-gitlab-ee#3959@2x/layers.json.js"] = [
	{
		"objectId": "2C75086A-682C-4C23-B759-3AF9FE51650A",
		"kind": "artboard",
		"name": "view1",
		"originalName": "view1",
		"maskFrame": null,
		"layerFrame": {
			"x": -1065,
			"y": -2720,
			"width": 1500,
			"height": 1812
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "4B0E7442-C754-43E3-8B54-1090661CD3F7",
				"kind": "group",
				"name": "config",
				"originalName": "config",
				"maskFrame": null,
				"layerFrame": {
					"x": 254,
					"y": 168,
					"width": 990,
					"height": 1516
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "6ECE3089-4CD7-4655-9028-A6BF00EF5FBE",
						"kind": "group",
						"name": "results",
						"originalName": "results",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 1585,
							"width": 990,
							"height": 99
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"children": [
							{
								"objectId": "DCBA1F69-BE66-4CB1-81DD-8EA4D2F8EDBA",
								"kind": "group",
								"name": "section_row",
								"originalName": "section_row",
								"maskFrame": null,
								"layerFrame": {
									"x": 254,
									"y": 1634,
									"width": 990,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-section_row-rencqtfg.png",
									"frame": {
										"x": 254,
										"y": 1634,
										"width": 990,
										"height": 50
									}
								},
								"children": []
							},
							{
								"objectId": "9467360C-5560-45CB-95AA-C718453CEA48",
								"kind": "group",
								"name": "section_header1",
								"originalName": "section_header",
								"maskFrame": null,
								"layerFrame": {
									"x": 254,
									"y": 1585,
									"width": 990,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-section_header-otq2nzm2.png",
									"frame": {
										"x": 254,
										"y": 1585,
										"width": 990,
										"height": 50
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "5D5B3B71-FB9C-480C-828C-7BAA9B8D3578",
						"kind": "group",
						"name": "section_footer",
						"originalName": "section_footer",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 1519,
							"width": 990,
							"height": 50
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_footer-nuq1qjnc.png",
							"frame": {
								"x": 254,
								"y": 1519,
								"width": 990,
								"height": 50
							}
						},
						"children": []
					},
					{
						"objectId": "8327726E-D80C-4205-9F98-3DA5EA78B85D",
						"kind": "group",
						"name": "section_trigger_pipelines",
						"originalName": "section_trigger_pipelines",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 1416,
							"width": 990,
							"height": 104
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_trigger_pipelines-odmynzcy.png",
							"frame": {
								"x": 254,
								"y": 1416,
								"width": 990,
								"height": 104
							}
						},
						"children": []
					},
					{
						"objectId": "EDEAC61C-E681-4C6D-92E8-666D4C5746CC",
						"kind": "group",
						"name": "section_mirror_direction",
						"originalName": "section_mirror_direction",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 1300,
							"width": 990,
							"height": 116
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_mirror_direction-rurfqum2.png",
							"frame": {
								"x": 254,
								"y": 1300,
								"width": 990,
								"height": 116
							}
						},
						"children": [
							{
								"objectId": "08BDC8C6-7167-4910-997B-ED776B70250E",
								"kind": "group",
								"name": "global_components_fields_label_input",
								"originalName": "global-components/fields/label + input",
								"maskFrame": null,
								"layerFrame": {
									"x": 271,
									"y": 1311,
									"width": 958,
									"height": 52
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_fields_label_input-mdhcrem4.png",
									"frame": {
										"x": 271,
										"y": 1311,
										"width": 958,
										"height": 52
									}
								},
								"children": [
									{
										"objectId": "333C85F0-5D81-4E28-AEC8-2A54BF1A9844",
										"kind": "group",
										"name": "icon",
										"originalName": "icon",
										"maskFrame": null,
										"layerFrame": {
											"x": 1209,
											"y": 1345,
											"width": 8,
											"height": 5
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-icon-mzmzqzg1.png",
											"frame": {
												"x": 1209,
												"y": 1345,
												"width": 8,
												"height": 5
											}
										},
										"children": []
									},
									{
										"objectId": "27879AD3-0F8C-4EFA-A026-FC2C46BBB6B9",
										"kind": "group",
										"name": "mirrordirectioninput",
										"originalName": "mirrordirectioninput",
										"maskFrame": null,
										"layerFrame": {
											"x": 271,
											"y": 1331,
											"width": 958,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-mirrordirectioninput-mjc4nzlb.png",
											"frame": {
												"x": 271,
												"y": 1331,
												"width": 958,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "39619781-C72D-457A-9E06-9BF85FA6CFCF",
						"kind": "group",
						"name": "section_host_keys_manual_input",
						"originalName": "section_host_keys_manual_input",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 1107,
							"width": 990,
							"height": 193
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_host_keys_manual_input-mzk2mtk3.png",
							"frame": {
								"x": 254,
								"y": 1107,
								"width": 990,
								"height": 193
							}
						},
						"children": [
							{
								"objectId": "664318FF-10B8-49BB-A14B-181B4706A361",
								"kind": "group",
								"name": "global_components_fields_label_textarea",
								"originalName": "global-components/fields/label + textarea",
								"maskFrame": null,
								"layerFrame": {
									"x": 270,
									"y": 1109,
									"width": 958,
									"height": 183
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_fields_label_textarea-njy0mze4.png",
									"frame": {
										"x": 270,
										"y": 1109,
										"width": 958,
										"height": 183
									}
								},
								"children": [
									{
										"objectId": "7C973876-20DD-4AB1-B791-EDA624A854EA",
										"kind": "group",
										"name": "sshinput",
										"originalName": "sshinput",
										"maskFrame": null,
										"layerFrame": {
											"x": 270,
											"y": 1130,
											"width": 958,
											"height": 162
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-sshinput-n0m5nzm4.png",
											"frame": {
												"x": 270,
												"y": 1130,
												"width": 958,
												"height": 162
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "1A4A15D1-A161-4B4A-BF01-8862A5CC0E21",
						"kind": "group",
						"name": "section_fingerprints",
						"originalName": "section_fingerprints",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 937,
							"width": 990,
							"height": 170
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_fingerprints-mue0qte1.png",
							"frame": {
								"x": 254,
								"y": 937,
								"width": 990,
								"height": 170
							}
						},
						"children": []
					},
					{
						"objectId": "C8919AE0-C816-47C2-9DE6-B1FD142E5532",
						"kind": "group",
						"name": "section_hostkeys",
						"originalName": "section_hostkeys",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 846,
							"width": 990,
							"height": 91
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_hostkeys-qzg5mtlb.png",
							"frame": {
								"x": 254,
								"y": 846,
								"width": 990,
								"height": 91
							}
						},
						"children": [
							{
								"objectId": "C9FCA80E-90DC-4779-9C56-7BDEF20A8C58",
								"kind": "group",
								"name": "button_detect",
								"originalName": "button_detect",
								"maskFrame": null,
								"layerFrame": {
									"x": 272,
									"y": 889,
									"width": 130,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_detect-qzlgq0e4.png",
									"frame": {
										"x": 272,
										"y": 889,
										"width": 130,
										"height": 32
									}
								},
								"children": []
							},
							{
								"objectId": "380C02F3-6739-4E5C-B419-825B9243B72D",
								"kind": "group",
								"name": "button_collapse",
								"originalName": "button_collapse",
								"maskFrame": null,
								"layerFrame": {
									"x": 410,
									"y": 889,
									"width": 202,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_collapse-mzgwqzay.png",
									"frame": {
										"x": 410,
										"y": 889,
										"width": 202,
										"height": 32
									}
								},
								"children": []
							},
							{
								"objectId": "8C822452-1B3D-46E0-888E-E001529CC4D9",
								"kind": "group",
								"name": "button_expand",
								"originalName": "button_expand",
								"maskFrame": null,
								"layerFrame": {
									"x": 410,
									"y": 889,
									"width": 180,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_expand-oem4mji0.png",
									"frame": {
										"x": 410,
										"y": 889,
										"width": 180,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "E53D64A5-3691-4C6B-9939-A22EBD8DDD02",
						"kind": "group",
						"name": "section_ssh",
						"originalName": "section_ssh",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 590,
							"width": 990,
							"height": 256
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_ssh-rtuzrdy0.png",
							"frame": {
								"x": 254,
								"y": 590,
								"width": 990,
								"height": 256
							}
						},
						"children": []
					},
					{
						"objectId": "219BA3FB-4EC1-4EDF-A616-B82AD2005A61",
						"kind": "group",
						"name": "section_username_password",
						"originalName": "section_username_password",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 508,
							"width": 990,
							"height": 82
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_username_password-mje5qkez.png",
							"frame": {
								"x": 254,
								"y": 508,
								"width": 990,
								"height": 82
							}
						},
						"children": [
							{
								"objectId": "6C7DD758-9462-47B1-B8A0-8BF1683BF218",
								"kind": "group",
								"name": "global_components_fields_label_textarea1",
								"originalName": "global-components/fields/label + textarea",
								"maskFrame": null,
								"layerFrame": {
									"x": 270,
									"y": 526,
									"width": 958,
									"height": 52
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_fields_label_textarea-nkm3req3.png",
									"frame": {
										"x": 270,
										"y": 526,
										"width": 958,
										"height": 52
									}
								},
								"children": [
									{
										"objectId": "32BDD681-D795-405B-B43D-C50C5E5B7A52",
										"kind": "group",
										"name": "passwordinput",
										"originalName": "passwordinput",
										"maskFrame": null,
										"layerFrame": {
											"x": 757,
											"y": 546,
											"width": 471,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-passwordinput-mzjcreq2.png",
											"frame": {
												"x": 757,
												"y": 546,
												"width": 471,
												"height": 32
											}
										},
										"children": []
									},
									{
										"objectId": "4855F3F9-7D19-465E-BEB9-3DE5AEF767CA",
										"kind": "group",
										"name": "usernameinput",
										"originalName": "usernameinput",
										"maskFrame": null,
										"layerFrame": {
											"x": 270,
											"y": 546,
											"width": 471,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-usernameinput-ndg1nuyz.png",
											"frame": {
												"x": 270,
												"y": 546,
												"width": 471,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "7F36554D-C8A0-43A0-85AD-04FABFB7674B",
						"kind": "group",
						"name": "section_authentication_method",
						"originalName": "section_authentication_method",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 442,
							"width": 990,
							"height": 66
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_authentication_method-n0yznju1.png",
							"frame": {
								"x": 254,
								"y": 442,
								"width": 990,
								"height": 66
							}
						},
						"children": [
							{
								"objectId": "88B2BE81-01F8-4D86-8CFF-11200478902A",
								"kind": "group",
								"name": "global_components_fields_label_textarea2",
								"originalName": "global-components/fields/label + textarea",
								"maskFrame": null,
								"layerFrame": {
									"x": 269,
									"y": 451,
									"width": 959,
									"height": 52
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_fields_label_textarea-odhcmkjf.png",
									"frame": {
										"x": 269,
										"y": 451,
										"width": 959,
										"height": 52
									}
								},
								"children": [
									{
										"objectId": "3E36BFCA-540B-4D34-8BCA-7CF55DC893AD",
										"kind": "group",
										"name": "icon1",
										"originalName": "icon",
										"maskFrame": null,
										"layerFrame": {
											"x": 1209,
											"y": 485,
											"width": 8,
											"height": 5
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-icon-m0uznkjg.png",
											"frame": {
												"x": 1209,
												"y": 485,
												"width": 8,
												"height": 5
											}
										},
										"children": []
									},
									{
										"objectId": "F34430CC-2C01-42BE-A315-21A750AF54B1",
										"kind": "group",
										"name": "authenticationinput",
										"originalName": "authenticationinput",
										"maskFrame": null,
										"layerFrame": {
											"x": 270,
											"y": 471,
											"width": 958,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-authenticationinput-rjm0ndmw.png",
											"frame": {
												"x": 270,
												"y": 471,
												"width": 958,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "3C9159D9-54E5-4E27-9213-C5777E06B5E0",
						"kind": "group",
						"name": "section_repo_url_description",
						"originalName": "section_repo_url_description",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 301,
							"width": 990,
							"height": 142
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_repo_url_description-m0m5mtu5.png",
							"frame": {
								"x": 254,
								"y": 301,
								"width": 990,
								"height": 142
							}
						},
						"children": []
					},
					{
						"objectId": "20F211B5-CC24-4D80-80B6-109778E746E1",
						"kind": "group",
						"name": "section_repo_url",
						"originalName": "section_repo_url",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 218,
							"width": 990,
							"height": 83
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_repo_url-mjbgmjex.png",
							"frame": {
								"x": 254,
								"y": 218,
								"width": 990,
								"height": 83
							}
						},
						"children": [
							{
								"objectId": "29C80287-5B4B-4824-8CFE-73E1CC63274A",
								"kind": "group",
								"name": "global_components_fields_label_textarea3",
								"originalName": "global-components/fields/label + textarea",
								"maskFrame": null,
								"layerFrame": {
									"x": 270,
									"y": 238,
									"width": 958,
									"height": 53
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-global_components_fields_label_textarea-mjldoday.png",
									"frame": {
										"x": 270,
										"y": 238,
										"width": 958,
										"height": 53
									}
								},
								"children": [
									{
										"objectId": "3AE299F3-B477-4E17-B748-D33D34F35BC7",
										"kind": "group",
										"name": "repourlinput",
										"originalName": "repourlinput",
										"maskFrame": null,
										"layerFrame": {
											"x": 270,
											"y": 259,
											"width": 958,
											"height": 32
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-repourlinput-m0ffmjk5.png",
											"frame": {
												"x": 270,
												"y": 259,
												"width": 958,
												"height": 32
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "18E053CE-320D-43B7-81A7-217F97202C05",
						"kind": "group",
						"name": "section_header",
						"originalName": "section_header",
						"maskFrame": null,
						"layerFrame": {
							"x": 254,
							"y": 168,
							"width": 990,
							"height": 50
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-section_header-mthfmduz.png",
							"frame": {
								"x": 254,
								"y": 168,
								"width": 990,
								"height": 50
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "65A04D7B-A4A1-4F2E-A188-67F3BE612F1E",
				"kind": "group",
				"name": "breadcrumbs",
				"originalName": "breadcrumbs",
				"maskFrame": null,
				"layerFrame": {
					"x": 255,
					"y": 16,
					"width": 990,
					"height": 32
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-breadcrumbs-njvbmdre.png",
					"frame": {
						"x": 255,
						"y": 16,
						"width": 990,
						"height": 32
					}
				},
				"children": [
					{
						"objectId": "608E1E74-3C8C-47A7-A528-418E5A17A304",
						"kind": "group",
						"name": "Group",
						"originalName": "Group",
						"maskFrame": null,
						"layerFrame": {
							"x": 255,
							"y": 16,
							"width": 400,
							"height": 16
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-Group-nja4rtff.png",
							"frame": {
								"x": 255,
								"y": 16,
								"width": 400,
								"height": 16
							}
						},
						"children": []
					}
				]
			},
			{
				"objectId": "237ABD1A-67A6-41B0-897B-D9ABF8E72652",
				"kind": "group",
				"name": "section_accordion_collapsed",
				"originalName": "section-accordion--collapsed",
				"maskFrame": null,
				"layerFrame": {
					"x": 255,
					"y": 80,
					"width": 990,
					"height": 65
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-section_accordion_collapsed-mjm3quje.png",
					"frame": {
						"x": 255,
						"y": 80,
						"width": 990,
						"height": 65
					}
				},
				"children": [
					{
						"objectId": "1C836141-A02D-45E5-90B6-E2FE064817F3",
						"kind": "group",
						"name": "expand_button",
						"originalName": "expand-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 1173,
							"y": 80,
							"width": 72,
							"height": 32
						},
						"visible": false,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-expand_button-mum4mzyx.png",
							"frame": {
								"x": 1173,
								"y": 80,
								"width": 72,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "DBBA592C-F918-4DFD-BDA7-0D5CE5FBBB32",
						"kind": "group",
						"name": "collapse_button",
						"originalName": "collapse-button",
						"maskFrame": null,
						"layerFrame": {
							"x": 1166,
							"y": 80,
							"width": 79,
							"height": 32
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-collapse_button-rejcqtu5.png",
							"frame": {
								"x": 1166,
								"y": 80,
								"width": 79,
								"height": 32
							}
						},
						"children": []
					},
					{
						"objectId": "F76FA511-5D9D-4674-A41E-258B47F9AC9B",
						"kind": "group",
						"name": "header",
						"originalName": "header",
						"maskFrame": null,
						"layerFrame": {
							"x": 255,
							"y": 85,
							"width": 846,
							"height": 60
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-header-rjc2rke1.png",
							"frame": {
								"x": 255,
								"y": 85,
								"width": 846,
								"height": 60
							}
						},
						"children": []
					}
				]
			}
		]
	}
]