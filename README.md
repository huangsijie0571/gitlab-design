# GitLab Design

:information_source: **This project is primarily used by [GitLab’s UX Design team][ux-handbook]
to host design files and hand them off for implementation. Before raising an
issue to any of GitLab’s issue trackers, please see [Getting help for GitLab](https://about.gitlab.com/getting-help/) on our
website to determine the best place to post. Thank you for helping to make GitLab a better product.**

![gitlab-cover-image](https://gitlab.com/gitlab-org/gitlab-design/raw/master/gitlab-cover-image.jpg)

- **[👀 View the pattern library Sketch file](/gitlab-pattern-library.sketch)**
- **[:arrow_upper_right: Browse specs and prototypes][design-pages]**

<!-- Table of contents generated with DocToc: https://github.com/thlorenz/doctoc -->
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [About](#about)
  - [Goals](#goals)
- [Getting started](#getting-started)
- [Contributing](#contributing)
- [Contacts](#contacts)
- [Links](#links)
- [License](#license)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About

**GitLab’s open source pattern library, prototypes, and work-in-progress files.**

This project is primarily used by [GitLab’s UX Design team][ux-handbook] to host design
files and hand them off for implementation. To learn about the best
practices to manage this project, including the repository’s organization,
check out the [contribution guidelines](/CONTRIBUTING.md). For more information
about the UX Design team, check out the [Links](#links) section.

### Goals

- Jumpstart design work by using the pattern library and previous work
- Enable frequent, stable, and consistent contributions
- Make GitLab’s design open, transparent, and open source
- Facilitate design handoffs and design–development communication design handoffs.

## Getting started

**If you’re a GitLab Inc. engineer**: you shouldn’t have to clone this project,
ever. Instead, ask the UX designer for the specs of the designs you’re working with.

**If you want to browse and contribute**:

1. The UX Design team primarily uses [Sketch](https://www.sketchapp.com/). See [Software](/CONTRIBUTING.md#software) for information on using alternatives.
1. Install and enable [Git Large File System (LFS)](https://about.gitlab.com/2017/01/30/getting-started-with-git-lfs-tutorial/):
   1. Install with [Homebrew](https://github.com/Homebrew/brew) via `brew install git-lfs` or [MacPorts](https://www.macports.org/) via `port install git-lfs`
   1. Enable with `git lfs install`
   - If you use a [Git GUI client](https://git-scm.com/download/gui/mac) (e.g. Tower) instead of the command line, look in the docs/manual of your app to see how you can install/enable Git LFS
1. Refer to the [contribution guidelines](/CONTRIBUTING.md) before contributing

Git LFS currently tracks the following file extensions on the repository:
`.atype`, `.sketch`, `.psd`, `.zip`, `.jpg`, `.png`, and `.pdf`.
An [archive of the repository before using Git LFS](https://gitlab.com/gitlab-org/gitlab-design-archive)
was created on July 8, 2017.

## Contributing

GitLab is an open source project and we are very happy to accept community
contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

## Contacts

Filter by the **UX** department on our [team page](https://about.gitlab.com/team/).

## Links

- [UX Handbook][ux-handbook]
- [UX Designer Onboarding](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/)
- [GitLab Design System](https://design.gitlab.com)
- [GitLab Design System Project](https://gitlab.com/gitlab-org/design.gitlab.com)
- [Gitlab Research Project](https://gitlab.com/gitlab-org/ux-research)
- [GitLab First Look](https://about.gitlab.com/community/gitlab-first-look/index.html)
- [GitLab Dribbble](https://dribbble.com/gitlab)
- [UX Guide **(deprecated)**](https://docs.gitlab.com/ce/development/ux_guide/)

## License

The GitLab Pattern Library is distributed under the MIT license, see the
[LICENSE](/LICENSE) for details.

[design-pages]: https://gitlab-org.gitlab.io/gitlab-design
[ux-handbook]: https://about.gitlab.com/handbook/engineering/ux/
