### Problem

(What’s the problem that this pattern solves? Why is it worth solving?)

### Solution

(What’s the solution? Why is it like that? What are the benefits?)

### Example(s)

(One or more images showing the UX pattern. They don’t have to be in GitLab.)

### Usage

(When do you use this pattern? And how?)

#### Dos and dont's

(Use this table to add images and text describing what’s ok and not ok.)

| :white_check_mark:  Do | :stop_sign: Don’t |
|------------------------|-------------------|
|  |  |

### Related patterns

(List any related or similar solutions. If none, write: No related patterns)

### Links / references

### Pattern checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit, if applicable.

1. [ ] Ensure that you have broken things down into atoms, molecules, and organisms.
1. [ ] Check that you have not created a duplicate of an existing pattern.
1. [ ] Ensure that you have used the proper method for creating the pattern depending on the complexity. Atoms and molecules are symbols, organisms are groups.
1. [ ] Make sure that typography is using text styles. When applicable used shared styles for colors.
1. [ ] QA check by another UXer
1. [ ] Add changes to the [pattern library](/gitlab-pattern-library.sketch)
1. [ ] Create an issue to add the pattern documentation to the [Design System](https://gitlab.com/gitlab-org/design.gitlab.com). Mark it as related to this issue.
1. [ ] Add an agenda item to the next UX weekly call to inform everyone (if new pattern, not yet used in the application)

/label ~"UX" ~"pattern library"
/cc @gitlab-com/gitlab-ux
